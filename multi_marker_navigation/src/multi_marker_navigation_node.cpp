/*
 * Copyright (C) 2008, Morgan Quigley and Willow Garage, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Stanford University or Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// %Tag(FULLTEXT)%
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <aruco_msgs/MarkerArray.h>
#include <sensor_msgs/PointCloud.h>
#include <std_msgs/Int16.h>
//#include <tf2/LinearMath/Matrix3x3.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/convert.h>
#include <tf2/transform_datatypes.h>
#include <tf2/buffer_core.h>
#include <tf2/convert.h>
#include <string>
#include <vector>
#include <iostream>
#include <std_msgs/UInt32MultiArray.h>

#define VISLAM_MAP_ROTATION 0//1.57

using namespace std;

int num_of_markers, mark_list;

ros::Publisher cloud_pub, marker_pose_pub, pose_map_pub, undist_pub,touch_down_flag, marker_cloud_pub;
//std::vector<ros::Publisher> marker_pose_pub;
geometry_msgs::PoseStamped vislam_pose;
sensor_msgs::PointCloud cloud, marker_cloud;
geometry_msgs::TransformStamped mapTransform_s, mapTransform_inv_s;

//fix in function
tf2::Transform map_trans,map_trans_inv;

unsigned int num_points = 300;
int cnt = 1;


#define MARKER_LIB_SIZE 10
int marker_scale[MARKER_LIB_SIZE] = {22/1, //Marker 0
                                    22/1, //Marker 1
                                    22/1, //Marker 2
                                    22/1, //Marker 3
                                    22/1, //Marker 4
                                    22/1, //Marker 5
                                    22/1, //Marker 6
                                    22/5, //Marker 7
                                    22/8, //Marker 8
                                    22/22}; //Marker 9
                                                           //X,Y,Z
tf2::Vector3 marker_map[MARKER_LIB_SIZE] = {tf2::Vector3(-0.24  ,0.0  ,0.0  ),  //Marker 0
                                            tf2::Vector3(-0.3  ,0.0  ,0.0  ),  //Marker 1
                                            tf2::Vector3(0.0  ,0.0  ,0.0  ),  //Marker 2
                                            tf2::Vector3(0.0  ,0.0  ,0.0  ),  //Marker 3
                                            tf2::Vector3(0.0  ,0.0  ,0.0  ),  //Marker 4
                                            tf2::Vector3(0.0  ,0.0  ,0.0  ),  //Marker 5
                                            tf2::Vector3(0.0  ,0.0  ,0.0  ),  //Marker 6
                                            tf2::Vector3(-0.03  ,0.0 ,0.05 ),  //Marker 7
                                            tf2::Vector3(-2.12  ,0.84 ,0.145  ),  //Marker 8
                                            tf2::Vector3(-0.24  ,0.0  ,0.12  )}; //Marker 9


/**
 * Using location marker detections for VISLAM absolute positioning
 */
// %Tag(CALLBACK)%


void pose_Callback(const geometry_msgs::PoseStamped msg)
{
    ros::Time frame_time;
    frame_time = msg.header.stamp;

   //float R_00 = R[0][0];
   //printf("R_00: %f\n",R_00 );
   float pos_z = msg.pose.position.z;
   //printf("pos_z: %f\n",pos_z );
   //printf("Quaternion:  %f,%f,%f,%f \n", q.x() , q.y() , q.z() , q.w() );  
   //printf("Quaternion_prev:  %f,%f,%f,%f \n", q_prev.x() , q_prev.y() , q_prev.z() , q_prev.w() );

   vislam_pose.pose = msg.pose;

   //Send final map transform
   static tf2_ros::TransformBroadcaster br;
   br.sendTransform(mapTransform_s);

   geometry_msgs::PoseStamped pose_map;

   pose_map.header.stamp = ros::Time::now();
   pose_map.header.frame_id = "map_frame";

   tf2::doTransform(vislam_pose, pose_map, mapTransform_inv_s);

   pose_map_pub.publish(pose_map);

}
void marker_list_cb(const std_msgs::UInt32MultiArray msg){
    mark_list = msg.data.size();
    cout << "*** "<< mark_list << " ***"<<endl;


}

void marker_Callback(const aruco_msgs::MarkerArray msg)
{   
    // touch_down_flag.publish(1);

    cout << "---------------------" << endl;
    num_of_markers = msg.markers.size();
    cout << "markers detected " << num_of_markers << endl;
    
    //declaring some variables out of for loop for accessing them later
    tf2::Vector3 map_inc_sum(0.0, 0.0, 0.0);
    tf2::Vector3 map_inc;
    geometry_msgs::PoseStamped marker_pose;
    // for loop starts here *************
    for (int i = 0; i < num_of_markers; i++)
    {
      
      int marker_id = msg.markers[i].id;
      geometry_msgs::PoseStamped undist;
      undist.pose.position.x=(msg.markers[i].pose.pose.position.x)/marker_scale[marker_id];
      undist.pose.position.y=(msg.markers[i].pose.pose.position.y)/marker_scale[marker_id];
      undist.pose.position.z=(msg.markers[i].pose.pose.position.z)/marker_scale[marker_id];

      undist.pose.orientation.x=msg.markers[i].pose.pose.orientation.x;
      undist.pose.orientation.y=msg.markers[i].pose.pose.orientation.y;
      undist.pose.orientation.z=msg.markers[i].pose.pose.orientation.z;
      undist.pose.orientation.w=msg.markers[i].pose.pose.orientation.w;

      cout << "undist  " << (msg.markers[i].pose.pose.position.x)/marker_scale[marker_id] << " " << (msg.markers[i].pose.pose.position.x)/marker_scale[marker_id] << " " << (msg.markers[i].pose.pose.position.x)/marker_scale[marker_id] << endl;

      tf2_ros::TransformBroadcaster tfb;
      geometry_msgs::TransformStamped transformStamped;

      tf2::Quaternion R1;
      R1.setRPY(0.7853981633,0.0,-1.57079632679);
      //R1=R1.inverse();


      transformStamped.header.frame_id = "dfc";
      transformStamped.child_frame_id = "undist";
      transformStamped.transform.translation.x = 0.0;
      transformStamped.transform.translation.y = 0.0;
      transformStamped.transform.translation.z = 0.0;
      tf2::Quaternion q1=R1;
      transformStamped.transform.rotation.x = q1.x();
      transformStamped.transform.rotation.y = q1.y();
      transformStamped.transform.rotation.z = q1.z();
      transformStamped.transform.rotation.w = q1.w();

      transformStamped.header.stamp = ros::Time::now();
      tfb.sendTransform(transformStamped);

      tf2::doTransform(undist, undist, transformStamped);
      undist_pub.publish(undist);

      float aruco_test = msg.header.seq;
      //cout << "aruco_test" << aruco_test << endl;


      //cout << "aruco_pos_x" << aruco_pos_x << endl;

      tf2::Quaternion q(undist.pose.orientation.x,undist.pose.orientation.y,undist.pose.orientation.z,undist.pose.orientation.w);

      tf2::Matrix3x3 R(q);//(undist.pose.orientation);
      double roll, pitch, yaw;
      tf2::Matrix3x3 R_inv = R.inverse();
      R_inv.getRPY(roll, pitch, yaw);
      tf2::Vector3 marker_pos(undist.pose.position.x,undist.pose.position.y,undist.pose.position.z);
      tf2::Vector3 vislam_pos(vislam_pose.pose.position.x,vislam_pose.pose.position.y,vislam_pose.pose.position.z);

      tf2::Vector3 cam_pos;
      //cam_pos = R_inv*(-marker_pos)+vislam_pos;//R_inv*(-marker_pos);

      tf2::Quaternion q_vislam(vislam_pose.pose.orientation.x,vislam_pose.pose.orientation.y,vislam_pose.pose.orientation.z,vislam_pose.pose.orientation.w);
      tf2::Matrix3x3 R_vislam(q_vislam);
      tf2::Matrix3x3 R_vislam_inv = R_vislam.inverse();
      cam_pos = R_vislam*(marker_pos)+vislam_pos;

      tf2::Quaternion q_inv;
      q_inv = q_vislam;

      //geometry_msgs::PoseStamped marker_pose[num_of_markers];

      marker_pose.header = msg.header;
      //marker_pose.pose = undist.pose;// = msg.pose;
      marker_pose.pose.position.x = cam_pos[0];
      marker_pose.pose.position.y = cam_pos[1];
      marker_pose.pose.position.z = cam_pos[2];
      marker_pose.pose.orientation.x = q_inv[0];
      marker_pose.pose.orientation.y = q_inv[1];
      marker_pose.pose.orientation.z = q_inv[2];
      marker_pose.pose.orientation.w = q_inv[3];

      // marker_pose_pub.publish(marker_pose);

      // sensor_msgs::PointCloud cloud;
      cloud.header.stamp = ros::Time::now();
      cloud.header.frame_id = "imu_start";
      cloud.points.resize(num_points);

      cloud.channels.resize(1);
      cloud.channels[0].name = "intensities";
      cloud.channels[0].values.resize(num_points);


      cloud.points[cnt-1].x = cam_pos[0];
      cloud.points[cnt-1].y = cam_pos[1];
      cloud.points[cnt-1].z = cam_pos[2];
      cloud.channels[0].values[cnt-1] = 100;// + cnt;

      // cout << "cam_pos: " << cam_pos[0] << " " << cam_pos[1] <<" "<< cam_pos[2] << endl;

      cloud_pub.publish(cloud);
      cnt++;
      if(cnt > num_points)cnt = 1;

      // cout << "RPY: " << roll*57 << " " << pitch*57 <<" "<< yaw*57 << endl;

      //cout << "R:" << R << endl;

      // static tf2_ros::TransformBroadcaster br;

      // tf2::Quaternion q_map;
      // q_map.setRPY(0, 0, 1.57);
      //
      // tf2::Vector3 map_pos(0.0,-2.6,0.0);
      //
      // mapTransform_s.header.stamp = ros::Time::now();
      // mapTransform_s.header.frame_id = "vislam";
      // mapTransform_s.child_frame_id = "map_frame";
      // mapTransform_s.transform.translation.x = map_pos.x();//0.0;
      // mapTransform_s.transform.translation.y = map_pos.y();//-2.6;
      // mapTransform_s.transform.translation.z = map_pos.z();//0.0;
      //
      // mapTransform_s.transform.rotation.x = q_map.x();
      // mapTransform_s.transform.rotation.y = q_map.y();
      // mapTransform_s.transform.rotation.z = q_map.z();
      // mapTransform_s.transform.rotation.w = q_map.w();

      //Transform marker position from vislam frame to map_frame
      geometry_msgs::PoseStamped marker_pose_map;
      // tf2::Transform map_trans,map_trans_inv;
      tf2::fromMsg(mapTransform_s.transform,map_trans);
      map_trans_inv = map_trans.inverse();

      mapTransform_inv_s = mapTransform_s;
      mapTransform_inv_s.transform = tf2::toMsg(map_trans_inv);

      tf2::doTransform(marker_pose, marker_pose_map, mapTransform_inv_s);

      marker_pose_pub.publish(marker_pose_map);
      
      marker_cloud.header.stamp = ros::Time::now();
      marker_cloud.header.frame_id = "imu_start";
      marker_cloud.points.resize(num_points);

      marker_cloud.channels.resize(1);
      marker_cloud.channels[0].name = "intensities";
      marker_cloud.channels[0].values.resize(num_points);


      marker_cloud.points[cnt-1].x = marker_pose_map.pose.position.x;
      marker_cloud.points[cnt-1].y = marker_pose_map.pose.position.y;
      marker_cloud.points[cnt-1].z = marker_pose_map.pose.position.z;
      marker_cloud.channels[0].values[cnt-1] = 100;// + cnt;
      marker_cloud_pub.publish(marker_cloud);
      //Calculate error from marker position lookup table
      
      cout << "marker_id " << marker_id << endl;
      cout << "marker_pose_map " << marker_pose_map.pose.position.x << " " << marker_pose_map.pose.position.y << " " << marker_pose_map.pose.position.z << endl;

      tf2::Vector3 marker_map_pos = marker_map[marker_id];
      tf2::Vector3 map_diff = marker_map_pos - tf2::Vector3(  marker_pose_map.pose.position.x,
                                                              marker_pose_map.pose.position.y,
                                                              marker_pose_map.pose.position.z);


      cout << "map_diff" << map_diff.x() << " " << map_diff.y() << " " << map_diff.z() << endl;

      //Rotate diff back into vislam frame
      tf2::Vector3 map_diff_vislam =  tf2::Matrix3x3(map_trans.getRotation())*map_diff;

      cout << "map_diff_vislam" << map_diff_vislam.x() << " " << map_diff_vislam.y() << " " << map_diff_vislam.z() << endl;

      //Increment map translation in next frame by error fraction
      float map_filter_gain = -0.5;
      map_inc = map_diff_vislam * map_filter_gain;


      map_inc_sum += map_inc;
      cout << "map_inc_sum " << map_inc_sum.x() << " " << map_inc_sum.y()<< " " << map_inc_sum.z() << endl;
      cout << "- - - -" << endl;
    } // for loop ends here ******************

    // geometry_msgs::PoseStamped marker_pose_map;
    // // tf2::Transform map_trans,map_trans_inv;
    // tf2::fromMsg(mapTransform_s.transform,map_trans);
    // map_trans_inv = map_trans.inverse();

    // mapTransform_inv_s = mapTransform_s;
    // mapTransform_inv_s.transform = tf2::toMsg(map_trans_inv);

    // tf2::doTransform(marker_pose, marker_pose_map, mapTransform_inv_s);
    // marker_pose_pub.publish(marker_pose_map);

    // find the average map_inc and simply update the value
    // cout << "map_inc x " << map_inc.x() << "  map_inc X " << map_inc.getX() << endl;
    if (num_of_markers ==0){
        map_inc.setX( 0);
        map_inc.setY( 0);
        map_inc.setZ( 0);
    } else{
          map_inc.setX( (map_inc_sum.x()/num_of_markers));
          map_inc.setY( (map_inc_sum.y()/num_of_markers));
          map_inc.setZ( (map_inc_sum.z()/num_of_markers));
    }
    cout << "map_inc xyz " << " " << map_inc.x() << " " << map_inc.y() << " " << map_inc.z() << endl;
    

    //Map frame rotation -> later calculate from point cloud or seperate source
    tf2::Quaternion q_map;
    q_map.setRPY(0, 0, VISLAM_MAP_ROTATION);

    mapTransform_s.header.stamp = ros::Time::now();

    mapTransform_s.header.frame_id = "imu_start";
    mapTransform_s.child_frame_id = "map_frame";

    mapTransform_s.transform.translation.x += map_inc.x();//0.0;
    mapTransform_s.transform.translation.y += map_inc.y();//-2.6;
    mapTransform_s.transform.translation.z += map_inc.z();//0.0;

    mapTransform_s.transform.rotation.x = q_map.x();
    mapTransform_s.transform.rotation.y = q_map.y();
    mapTransform_s.transform.rotation.z = q_map.z();
    mapTransform_s.transform.rotation.w = q_map.w();

    //initial inverse Duplicate!! make function!
    //tf2::Transform map_trans,map_trans_inv;
    tf2::fromMsg(mapTransform_s.transform,map_trans);
    map_trans_inv = map_trans.inverse();

    mapTransform_inv_s = mapTransform_s;
    mapTransform_inv_s.transform = tf2::toMsg(map_trans_inv);
    //Duplicate!! make function!


    // //Send final map transform
    // br.sendTransform(mapTransform_s);

}

// %EndTag(CALLBACK)%

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "marker_nav");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
   ros::NodeHandle n;

  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * The second parameter to the subscribe() function is the size of the message
   * queue.  If messages are arriving faster than they are being processed, this
   * is the number of messages that will be buffered up before beginning to throw
   * away the oldest ones.
   */
// %Tag(SUBSCRIBER)%
  ros::Subscriber sub, sub_2, sub_3;

  sub = n.subscribe("downward/vio/pose", 1, pose_Callback);
  sub_2 = n.subscribe("aruco_marker_publisher/markers", 1, marker_Callback);
  sub_3 = n.subscribe("aruco_marker_publisher/markers_list", 1, marker_list_cb);
  
// %EndTag(SUBSCRIBER)%

  cloud_pub = n.advertise<sensor_msgs::PointCloud>("cloud",50);
  marker_cloud_pub = n.advertise<sensor_msgs::PointCloud>("cloud_of_marker",50);
  // marker_pose_pub = n.advertise<geometry_msgs::PoseStamped>("marker_in_vislam_frame",50);
  pose_map_pub = n.advertise<geometry_msgs::PoseStamped>("pose_in_map_frame",50);
  undist_pub = n.advertise<geometry_msgs::PoseStamped>("undistorted",50);
  touch_down_flag=n.advertise<std_msgs::Int16>("tdf",1);

  // ros::Publisher cloud_pub = n.advertise<sensor_msgs::PointCloud>("cloud",50);
  // unsigned int num_points = 100;
  // int count = 0;
  //ros::Rate r(1.0);

  //while(cnt<num_points){
    // sensor_msgs::PointCloud cloud;
    // cloud.header.stamp = ros::Time::now();
    // cloud.header.frame_id = "sensor_frame";
    // cloud.points.resize(num_points);
    //
    // cloud.channels.resize(1);
    // cloud.channels[0].name = "intensities";
    // cloud.channels[0].values.resize(num_points);
    //
    // for(unsigned int i = 0; i < num_points;i++){
    //     cloud.points[i].x = 1 + cnt;
    //     cloud.points[i].y = 2 + cnt;
    //     cloud.points[i].z = 3 + cnt;
    //     cloud.channels[0].values[i] = 100 + cnt;
    // }
    //
    // cloud_pub.publish(cloud);
    // cnt++;
    //r.sleep();

  //}


  //Init map transform
  tf2::Quaternion q_map_init;
  q_map_init.setRPY(0, 0, VISLAM_MAP_ROTATION);

  tf2::Vector3 map_pos_init(0.0,0.0,0.0);

  mapTransform_s.header.stamp = ros::Time::now();
  mapTransform_s.header.frame_id = "imu_start";
  mapTransform_s.child_frame_id = "map_frame";
  mapTransform_s.transform.translation.x = map_pos_init.x();//0.0;
  mapTransform_s.transform.translation.y = map_pos_init.y();//-2.6;
  mapTransform_s.transform.translation.z = map_pos_init.z();//0.0;

  mapTransform_s.transform.rotation.x = q_map_init.x();
  mapTransform_s.transform.rotation.y = q_map_init.y();
  mapTransform_s.transform.rotation.z = q_map_init.z();
  mapTransform_s.transform.rotation.w = q_map_init.w();

  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */
// %Tag(SPIN)%
  ros::spin();
// %EndTag(SPIN)%

  return 0;
}
// %EndTag(FULLTEXT)%
