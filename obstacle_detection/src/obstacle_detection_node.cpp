
// %Tag(FULLTEXT)%
#include <obstacle_detection.h>

obstDet::obstDet(ros::NodeHandle& nh):
tf_listener_(tf_buffer_),
marker_scale_{39.0/30.0, //Marker 1
              39.0/100.0, //Marker 3
              39.0/50.0, //Marker 5
              39.0/1000.0, //Marker 7
              39.0/22.0, //Marker 9
              39.0/100.0, //Marker 11
              39.0/100.0, //Marker 13
              39.0/100.0, //Marker 15
              39.0/100.0, //Marker 17
              39.0/100.0, //Marker 19
              39.0/100.0, //Marker 21
              39.0/100.0, //Marker 23
              39.0/100.0, //Marker 25
              39.0/30.0, //Marker 27
              39.0/50.0, //Marker 29
              39.0/100.0, //Marker 31
              39.0/100.0, //Marker 33
              39.0/100.0, //Marker 35
              39.0/100.0, //Marker 37
              39.0/100.0, //Marker 39
              39.0/30.0, //Marker 41
              39.0/30.0, //Marker 43
              39.0/30.0, //Marker 45
              39.0/30.0, //Marker 47
              39.0/100.0, //Marker 49
              39.0/50.0, //Marker 51
              39.0/100.0, //Marker 53
              39.0/100.0, //Marker 55
              39.0/40.0, //Marker 57
              39.0/100.0, //Marker 59
              39.0/30.0, //Marker 61
              39.0/30.0, //Marker 63
              39.0/30.0, //Marker 65
              39.0/30.0, //Marker 67
              39.0/100.0, //Marker 69
              39.0/100.0, //Marker 71
              39.0/100.0, //Marker 73
              39.0/100.0, //Marker 75
              39.0/100.0, //Marker 77
              39.0/100.0, //Marker 79
              39.0/100.0, //Marker 81
              39.0/100.0, //Marker 83
              39.0/100.0, //Marker 85
              39.0/100.0, //Marker 87
              39.0/100.0, //Marker 89
              39.0/100.0, //Marker 91
              39.0/100.0, //Marker 93
              39.0/100.0, //Marker 95
              39.0/100.0, //Marker 97
              39.0/100.0, //Marker 99
              39.0/100.0, //Marker 101
              39.0/100.0, //Marker 103
              39.0/100.0, //Marker 105
              39.0/50.0, //Marker 107
              39.0/30.0, //Marker 109
              39.0/30.0, //Marker 111
              39.0/30.0, //Marker 113
              39.0/30.0, //Marker 115
              39.0/30.0, //Marker 117
              39.0/30.0}, //Marker 119
                    

                        // X      Y     Z
marker_map_{tf2::Vector3(0.00  ,-0.15  ,-0.15  ),  //Marker 1
            tf2::Vector3(0.00  ,-0.5   ,-0.5  ),  //Marker 3
            tf2::Vector3(0.00  ,-0.25  ,3.25  ),  //Marker 5
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 7
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 9
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 11
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 13
            tf2::Vector3(0.0   ,-0.5   ,-0.5 ),  //Marker 15
            tf2::Vector3(0.0   ,-0.5   ,0.5  ),  //Marker 17
            tf2::Vector3(0.0   ,-0.5   ,0.5  ),  //Marker 19
            tf2::Vector3(0.0   ,-0.5   ,0.5  ),  //Marker 21
            tf2::Vector3(0.0   ,-0.5   ,0.5  ),  //Marker 23
            tf2::Vector3(0.0   ,-0.5   ,0.5  ),  //Marker 25
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 27
            tf2::Vector3(0.0   ,-0.25  ,-0.25  ),  //Marker 29
            tf2::Vector3(0.0   ,-0.25  ,-0.25  ),  //Marker 31
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 33
            tf2::Vector3(0.0   ,-0.5   ,3.5  ),  //Marker 35
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 37
            tf2::Vector3(0.0   ,-0.2   ,-0.2  ),  //Marker 39
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 41
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 43
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 45
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 47
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 49
            tf2::Vector3(0.0   ,-0.25  ,-0.25  ),  //Marker 51
            tf2::Vector3(0.0   ,-0.5   ,-2.5  ),  //Marker 53
            tf2::Vector3(0.0   ,-0.5   ,-2.5  ),  //Marker 55
            tf2::Vector3(0.0   ,-0.2   ,-0.2  ),  //Marker 57
            tf2::Vector3(0.0   ,-0.5   ,2.5  ),  //Marker 59
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 61
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 63
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 65
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 67
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 69
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 71
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 73
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 75
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 77
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 79
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 81
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 83
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 85
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 87
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 89
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 91
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 93
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 95
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 97
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 99
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 101
            tf2::Vector3(0.0   ,-0.5   ,-0.5  ),  //Marker 103
            tf2::Vector3(0.0   ,-0.25  ,2.0  ),  //Marker 105
            tf2::Vector3(0.0   ,-0.25  ,-0.25  ),  //Marker 107
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 109
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 111
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 113
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 115
            tf2::Vector3(0.0   ,-0.15  ,-0.15  ),  //Marker 117
            tf2::Vector3(0.0   ,-0.15  ,-0.15  )},  //Marker 119





                      // Width, Height, Depth
obst_sizes_{tf2::Vector3(0.015  ,0.05  ,0.015  ),  //Marker 1
            tf2::Vector3(3.0    ,5.0   ,7.0  ),  //Marker 3
            tf2::Vector3(6.0    ,6.0   ,6.0  ),  //Marker 5
            tf2::Vector3(1.5    ,4.5   ,10.0  ),  //Marker 7
            tf2::Vector3(0.5    ,1.5   ,3.0  ),  //Marker 9
            tf2::Vector3(1.5    ,4.5   ,30.0  ),  //Marker 11
            tf2::Vector3(1.5    ,4.5   ,30.0  ),  //Marker 13
            tf2::Vector3(1.5    ,4.5   ,30.0 ),  //Marker 15
            tf2::Vector3(1.5    ,4.5   ,30.0 ),  //Marker 17
            tf2::Vector3(1.5    ,4.5   ,30.0 ),  //Marker 19
            tf2::Vector3(1.5    ,4.5   ,30.0 ),  //Marker 21
            tf2::Vector3(1.5    ,4.5   ,30.0 ),  //Marker 23
            tf2::Vector3(1.5    ,4.5   ,30.0 ),  //Marker 25
            tf2::Vector3(0.125  ,0.09  ,0.3 ),  //Marker 27
            tf2::Vector3(1.3    ,1.8   ,4.5 ),  //Marker 29
            tf2::Vector3(1.3    ,1.8   ,4.5 ),  //Marker 31
            tf2::Vector3(3.125  ,1.5   ,12.0 ),  //Marker 33
            tf2::Vector3(5.0    ,5.0   ,5.0 ),  //Marker 35
            tf2::Vector3(3.0    ,7.0   ,9.0 ),  //Marker 37
            tf2::Vector3(1.0    ,1.0   ,1.0 ),  //Marker 39
            tf2::Vector3(0.15   ,0.19  ,0.4 ),  //Marker 41
            tf2::Vector3(0.15   ,0.19  ,0.4 ),  //Marker 43
            tf2::Vector3(0.15   ,0.19  ,0.4 ),  //Marker 45
            tf2::Vector3(0.15   ,0.19  ,0.4 ),  //Marker 47
            tf2::Vector3(14.0   ,3.0   ,15.0 ),  //Marker 49
            tf2::Vector3(.50    ,1.5   ,3.0 ),  //Marker 51
            tf2::Vector3(0.5    ,0.25  ,0.5 ),  //Marker 53
            tf2::Vector3(1.5    ,1.8   ,1.5 ),  //Marker 55
            tf2::Vector3(0.75   ,0.75  ,1.25 ),  //Marker 57
            tf2::Vector3(3.75   ,1.0   ,3.25 ),  //Marker 59
            tf2::Vector3(0.125  ,0.09  ,0.3 ),  //Marker 61
            tf2::Vector3(0.125  ,0.09  ,0.3 ),  //Marker 63
            tf2::Vector3(0.125  ,0.09  ,0.3 ),  //Marker 65
            tf2::Vector3(0.125  ,0.09  ,0.3 ),  //Marker 67
            tf2::Vector3(1.0    ,3.75  ,2.0 ),  //Marker 69
            tf2::Vector3(1.0    ,3.75  ,2.0 ),  //Marker 71
            tf2::Vector3(1.0    ,3.75  ,2.0 ),  //Marker 73
            tf2::Vector3(1.0    ,3.75  ,2.0 ),  //Marker 75
            tf2::Vector3(1.0    ,3.75  ,2.0 ),  //Marker 77
            tf2::Vector3(1.0    ,3.75  ,2.0 ),  //Marker 79
            tf2::Vector3(1.0    ,3.75  ,2.0 ),  //Marker 81
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 83
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 85
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 87
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 89
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 91
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 93
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 95
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 97
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 99
            tf2::Vector3(2.0    ,6.0   ,2.0 ),  //Marker 101
            tf2::Vector3(4.0    ,1.2   ,4.0 ),  //Marker 103
            tf2::Vector3(3.0    ,3.0   ,3.0 ),  //Marker 105
            tf2::Vector3(0.125  ,0.5   ,1.0 ),  //Marker 107
            tf2::Vector3(0.19   ,0.25  ,1.25  ),  //Marker 109
            tf2::Vector3(0.19   ,0.25  ,1.25  ),  //Marker 111
            tf2::Vector3(0.19   ,0.25  ,1.25  ),  //Marker 113
            tf2::Vector3(0.19   ,0.25  ,1.25  ),  //Marker 115
            tf2::Vector3(0.19   ,0.25  ,1.25  ),  //Marker 117
            tf2::Vector3(0.19   ,0.25  ,1.25  )}  //Marker 119

{
    nh_ = nh;

    // sub_pose = nh_.subscribe("downward/vio/pose", 1, poseCallback);
    sub_aruco_ = nh_.subscribe("aruco_marker_publisher/markers", 1, &obstDet::obstacleCallback, this);

    pub_pcl_ = nh_.advertise<sensor_msgs::PointCloud>("cloud_of_obst",50);
    pub_test_ = nh_.advertise<sensor_msgs::PointCloud>("cloud_of_testpts",50);

    density_= 10;


}

obstDet* obstDet::create(ros::NodeHandle& nh)
{
    obstDet* o_d = new obstDet(nh);
    return o_d;
}



void obstDet::obstacleCallback(const aruco_msgs::MarkerArray msg)
{
  
  num_of_markers_ = msg.markers.size();


  for (int k = 0; k < num_of_markers_; k++){
    
    int marker_id_actual = msg.markers[k].id;
    int marker_id = (marker_id_actual/2);
    
    geometry_msgs::PoseStamped undist; // marker pose in the camera frame
    undist.pose.position.x=(msg.markers[k].pose.pose.position.x)/marker_scale_[marker_id];
    undist.pose.position.y=(msg.markers[k].pose.pose.position.y)/marker_scale_[marker_id];
    undist.pose.position.z=(msg.markers[k].pose.pose.position.z)/marker_scale_[marker_id];

    undist.pose.orientation.x=msg.markers[k].pose.pose.orientation.x;
    undist.pose.orientation.y=msg.markers[k].pose.pose.orientation.y;
    undist.pose.orientation.z=msg.markers[k].pose.pose.orientation.z;
    undist.pose.orientation.w=msg.markers[k].pose.pose.orientation.w;

    tf2::Quaternion q_mark_cam(undist.pose.orientation.x,undist.pose.orientation.y,undist.pose.orientation.z,undist.pose.orientation.w);
    tf2::Matrix3x3 R_mark_cam(q_mark_cam);
    double rollc, pitchc, yawc;
    R_mark_cam.getRPY(rollc, pitchc, yawc);
    
    // rotating from marker frame to camera frame
    tf2_ros::TransformBroadcaster tfb0;
    geometry_msgs::TransformStamped transformStamped0;

    transformStamped0.header.frame_id = "camera_infra1_optical_frame";
    transformStamped0.child_frame_id = "marker"; //undist
    transformStamped0.transform.translation.x = (msg.markers[k].pose.pose.position.x)/marker_scale_[marker_id];
    transformStamped0.transform.translation.y = (msg.markers[k].pose.pose.position.y)/marker_scale_[marker_id];
    transformStamped0.transform.translation.z = (msg.markers[k].pose.pose.position.z)/marker_scale_[marker_id];
    
    transformStamped0.transform.rotation.x = msg.markers[k].pose.pose.orientation.x;
    transformStamped0.transform.rotation.y = msg.markers[k].pose.pose.orientation.y;
    transformStamped0.transform.rotation.z = msg.markers[k].pose.pose.orientation.z;
    transformStamped0.transform.rotation.w = msg.markers[k].pose.pose.orientation.w;

    transformStamped0.header.stamp = msg.header.stamp;
    tfb0.sendTransform(transformStamped0);  


    geometry_msgs::TransformStamped transformStamped1;
    try{
          transformStamped1 = tf_buffer_.lookupTransform("world", "camera_infra1_optical_frame",ros::Time(0));
        }
    catch (tf2::TransformException &ex) {
      ROS_WARN("%s",ex.what());
      ros::Duration(1.0).sleep();
      continue;
    }

    tf2::Vector3 test_vec = tf2::Vector3(0.0,0.0,-1.0); // test vector in the marker frame
    tf2::Quaternion mark(undist.pose.orientation.x,undist.pose.orientation.y,undist.pose.orientation.z,undist.pose.orientation.w);
    tf2::Matrix3x3 R_mark(mark);
    
    tf2::Vector3 test_vec_camera_rotation = R_mark*test_vec;

    tf2::Vector3 mark_to_cam_trans = tf2::Vector3(undist.pose.position.x,undist.pose.position.y,undist.pose.position.z);

    tf2::Vector3 test_vec_camera = test_vec_camera_rotation + mark_to_cam_trans;
    geometry_msgs::Vector3 test_vec_cam = tf2::toMsg(test_vec_camera);

    geometry_msgs::Vector3 test_out_geo; // in the world frame
    tf2::doTransform(test_vec_cam, test_out_geo, transformStamped1);

    tf2::Vector3 test_out;
    tf2::fromMsg(test_out_geo, test_out);
    
    float testx = test_out[0];
    float testy = test_out[1];
    float testz = test_out[2];
    testz = 0; // for using the projection of the vector on x-y plane
    
    
    tf2::Vector3 map_vec = tf2::Vector3(0.0,1.0,0.0);
    float dot_prod = map_vec.getX()*testx + map_vec.getY()*testy + map_vec.getZ()*testz;

    float abs_testout = sqrt(testx*testx + testy*testy + testz*testz);
    float abs_mapvec = sqrt(map_vec.getX()*map_vec.getX() + map_vec.getY()*map_vec.getY() + map_vec.getZ()*map_vec.getZ());
    
    float verify_angle_abs = acos(dot_prod/(abs_testout*abs_mapvec));
    

    float vcp_i = map_vec.getY()*testz - map_vec.getZ()*testy;
    float vcp_j = map_vec.getZ()*testx - map_vec.getX()*testz;
    float vcp_k = map_vec.getX()*testy - map_vec.getY()*testx;

    
    float vcp_mag = sqrt(pow(vcp_i,2) + pow(vcp_j,2) + pow(vcp_k,2));  

    float final_ang;
    if(vcp_k/vcp_mag > 0){
      final_ang= verify_angle_abs;
    }
    else{
      final_ang= -verify_angle_abs;
    }
    std::cout << "Final angle: " << final_ang*180/3.14 << std::endl;
    
    geometry_msgs::PoseStamped marker_pose; // transforming from marker frame to world frame
    tf2::doTransform(undist, marker_pose, transformStamped1);
    
    // this is where the box front face coincides with world origin
                                        //  cordinates of the front face of the obstacle with its CoM at origin

    tf2::Vector3 bot_left = tf2::Vector3(0.0,
                                        0.0,
                                        0.0 )                       + tf2::Vector3(- obst_sizes_[marker_id].x()/2,
                                                                                   0.0, 
                                                                                  - obst_sizes_[marker_id].y()/2);
    // not used anywhere, but still computed
    tf2::Vector3 bot_right = tf2::Vector3(0.0,
                                         0.0,
                                          0.0 )                      + tf2::Vector3(obst_sizes_[marker_id].x()/2,
                                                                                     0.0, 
                                                                                    - obst_sizes_[marker_id].y()/2);

    tf2::Vector3 bot_left_in_Y = tf2::Vector3(bot_left.x() , bot_left.y() + obst_sizes_[marker_id].z() , bot_left.z());                                                                                    

    // increments along each axis
    float x_incr = obst_sizes_[marker_id].x()/density_;
    float y_incr = obst_sizes_[marker_id].z()/density_;
    float z_incr = obst_sizes_[marker_id].y()/density_;
       

    int arr_size = 2*((density_+1)*(density_+1)) + 2*((density_-1)*(density_+1)) + 2*((density_-1)*(density_-1));// + ((density_-1)*(density_));
    // int arr_size = density_+1 + density_ + density_-1;
    geometry_msgs::PoseStamped obst_pose[arr_size];

      
    // publishing test points
    test_cloud_.header.stamp = ros::Time::now();
    test_cloud_.header.frame_id = "world";

    test_cloud_.points.resize(2);

    test_cloud_.channels.resize(1);
    test_cloud_.channels[0].name = "some_name";
    test_cloud_.channels[0].values.resize(2);
    tf2::Vector3 marker_position = tf2::Vector3(marker_pose.pose.position.x,
                                  marker_pose.pose.position.y,
                                  marker_pose.pose.position.z );
    test_cloud_.points[0].x = test_out[0];
    test_cloud_.points[0].y = test_out[1];
    test_cloud_.points[0].z = test_out[2];
    test_cloud_.points[1].x = marker_position[0];
    test_cloud_.points[1].y = marker_position[1];
    test_cloud_.points[1].z = marker_position[2];
    pub_test_.publish(test_cloud_);

    int count = 0;


    // doing the point cloud thing 
    // filling x-z plane containing the bot_left point
    float bot_left_Z = bot_left.z();
    float bot_left_x = bot_left.x();
    float bot_left_y = bot_left.y();
    
    
    for(int i=0; i<density_+1; i++){
      float bot_left_x = bot_left.x();

      for(int j=0; j<density_+1; j++){
        obst_pose[count].pose.position.x = bot_left_x;//bot_left.x() + x_incr*(j+1);
        obst_pose[count].pose.position.y = bot_left_y;
        obst_pose[count].pose.position.z = bot_left_Z;
        obst_pose[count].pose.orientation.x = 0.0;
        obst_pose[count].pose.orientation.y = 0.0;
        obst_pose[count].pose.orientation.z = 0.0;
        obst_pose[count].pose.orientation.w = 1.0;
        bot_left_x = bot_left_x + x_incr;
        count++;
      }
        bot_left_Z = bot_left_Z + z_incr;
    }
    // std::cout << "count " << count << std::endl;
      
     
    // filling x-z plane containing the bot_left_in_Y point
    bot_left_Z = bot_left.z(); 
    
    for(int i=0; i<density_+1; i++){
      bot_left_x = bot_left.x();  
      
      for(int j=0; j<density_+1; j++){
        obst_pose[count].pose.position.x = bot_left_x;//bot_left.x() + x_incr*(j+1);
        obst_pose[count].pose.position.y = bot_left_in_Y.y();
        obst_pose[count].pose.position.z = bot_left_Z;
        obst_pose[count].pose.orientation.x = 0.0;
        obst_pose[count].pose.orientation.y = 0.0;
        obst_pose[count].pose.orientation.z = 0.0;
        obst_pose[count].pose.orientation.w = 1.0;
        bot_left_x = bot_left_x + x_incr;
        count++;
      }
        bot_left_Z = bot_left_Z + z_incr;
    }
    // std::cout << "count " << count << std::endl;


    // filling y-z plane containing the bot_left point
    bot_left_Z = bot_left.z();
    bot_left_y = bot_left.y();
    bot_left_x = bot_left.x();
        
    for(int i=0; i<density_+1; i++){
      bot_left_y = bot_left.y();

      for(int j=0; j<density_-1; j++){    
        bot_left_y = bot_left_y + y_incr;
        obst_pose[count].pose.position.x = bot_left_x;
        obst_pose[count].pose.position.y = bot_left_y;//bot_left.y() - y_incr*(j+1);
        obst_pose[count].pose.position.z = bot_left_Z;
        obst_pose[count].pose.orientation.x = 0.0;
        obst_pose[count].pose.orientation.y = 0.0;
        obst_pose[count].pose.orientation.z = 0.0;
        obst_pose[count].pose.orientation.w = 1.0;
        count++;
      }
        bot_left_Z = bot_left_Z + z_incr;
    }
    // std::cout << "count " << count << std::endl;


    // filling y-z plane containing the bot_right point
    bot_left_Z = bot_left.z();  
        
    for(int i=0; i<density_+1; i++){
      bot_left_y = bot_left.y();
      
      for(int j=0; j<density_-1; j++){    
        bot_left_y = bot_left_y + y_incr;
        obst_pose[count].pose.position.x = bot_left.x() + obst_sizes_[marker_id].x();
        obst_pose[count].pose.position.y = bot_left_y;//bot_left.y() - y_incr*(j+1);
        obst_pose[count].pose.position.z = bot_left_Z;
        obst_pose[count].pose.orientation.x = 0.0;
        obst_pose[count].pose.orientation.y = 0.0;
        obst_pose[count].pose.orientation.z = 0.0;
        obst_pose[count].pose.orientation.w = 1.0;
        count++;
      }
        bot_left_Z = bot_left_Z + z_incr;
    }
    // std::cout << "count " << count << std::endl;


    // filling x-y plane containing the bot_left point
    bot_left_y = bot_left.y(); 
        
    for(int i=0; i<density_-1; i++){
      bot_left_y = bot_left_y + y_incr;
      bot_left_x = bot_left.x();
    
      for(int j=0; j<density_-1; j++){
        bot_left_x = bot_left_x + x_incr;
        obst_pose[count].pose.position.x = bot_left_x;
        obst_pose[count].pose.position.y = bot_left_y;
        obst_pose[count].pose.position.z = bot_left.z();
        obst_pose[count].pose.orientation.x = 0.0;
        obst_pose[count].pose.orientation.y = 0.0;
        obst_pose[count].pose.orientation.z = 0.0;
        obst_pose[count].pose.orientation.w = 1.0;
        
        count++;
      }
    }
    // std::cout << "count " << count << std::endl;


    // filling x-y plane containing the top_left point
    bot_left_y = bot_left.y();    
        
    for(int i=0; i<density_-1; i++){
      bot_left_y = bot_left_y + y_incr;
      bot_left_x = bot_left.x();
    
      for(int j=0; j<density_-1; j++){
        bot_left_x = bot_left_x + x_incr;
        obst_pose[count].pose.position.x = bot_left_x;
        obst_pose[count].pose.position.y = bot_left_y;
        obst_pose[count].pose.position.z = bot_left.z() + obst_sizes_[marker_id].y();
        obst_pose[count].pose.orientation.x = 0.0;
        obst_pose[count].pose.orientation.y = 0.0;
        obst_pose[count].pose.orientation.z = 0.0;
        obst_pose[count].pose.orientation.w = 1.0;
        count++;
      }
        
    }
    // std::cout << "count " << count << std::endl;


    tf2::Vector3 obst_global_pos;
    

    // rotation of the obstacle position to orient it along marker
    tf2::Quaternion q_mark(marker_pose.pose.orientation.x,marker_pose.pose.orientation.y,marker_pose.pose.orientation.z,marker_pose.pose.orientation.w);

    tf2::Matrix3x3 R_markglobal(q_mark);
    double roll, pitch, yaw;
    R_markglobal.getRPY(roll, pitch, yaw);

    tf2::Quaternion p_mark;
    p_mark.setRPY(0.0,0.0,final_ang);
    tf2::Matrix3x3 p_yawRot(p_mark); // rotation matrix considering only yaw angle of the marker
 
    // doing the point cloud thing   
    obst_cloud_.header.stamp = ros::Time::now();
    obst_cloud_.header.frame_id = "world";

    obst_cloud_.points.resize(arr_size);

    obst_cloud_.channels.resize(1);
    obst_cloud_.channels[0].name = "intensities";
    obst_cloud_.channels[0].values.resize(arr_size);
    
    for(int i=0; i < arr_size; i++){
      
      tf2::Vector3 obst_pos(obst_pose[i].pose.position.x, obst_pose[i].pose.position.y, obst_pose[i].pose.position.z);
      obst_global_pos = p_yawRot*obst_pos; // only yaw rotation of the obstacle in the box frame
      
      
      obst_cloud_.points[i].x = obst_global_pos[0] + marker_pose.pose.position.x;
      obst_cloud_.points[i].y = obst_global_pos[1] + marker_pose.pose.position.y;
      obst_cloud_.points[i].z = obst_global_pos[2] + marker_pose.pose.position.z;
    }

    pub_pcl_.publish(obst_cloud_);

    int obst_pose_size = sizeof(obst_pose)/sizeof(obst_pose[0]);//obst_pose.size();
    

  } // main for loop ends here.
  
}


// %EndTag(CALLBACK)%

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "obstacle_det");
  ros::NodeHandle nh;

  obstDet* obstPub = NULL;

  obstPub = obstDet::create(nh);


  ros::spin();

  return 0;
}
