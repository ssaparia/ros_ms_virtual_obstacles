#ifndef OBSTACLE_DETECTION_H
#define OBSTACLE_DETECTION_H

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <aruco_msgs/MarkerArray.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <std_msgs/Int16.h>
//#include <tf2/LinearMath/Matrix3x3.h>
// #include <tf2/LinearMath/Scalar.h>
#include <stdlib.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/convert.h>
#include <tf2/transform_datatypes.h>
#include <tf2/buffer_core.h>
#include <tf2/convert.h>
// #include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <string>
#include <vector>
#include <iostream>
#include <std_msgs/UInt32MultiArray.h>
// #include <tf.h>
// #include <transform_datatypes.h>

const int MARKER_LIB_SIZE = 120;

class obstDet
{
    public:

        obstDet(ros::NodeHandle& nh);
        static obstDet* create(ros::NodeHandle& nh);
        ~obstDet();

    private:

        ros::NodeHandle nh_;

        ros::Subscriber sub_aruco_; //sub_pose_
        ros::Publisher pub_pcl_;
        ros::Publisher pub_test_;

        // geometry_msgs::PoseStamped vio_pose_;
        sensor_msgs::PointCloud obst_cloud_;
        sensor_msgs::PointCloud test_cloud_;

        tf2_ros::Buffer tf_buffer_;
        tf2_ros::TransformListener tf_listener_;

        int num_of_markers_, mark_list_;

        int density_;

        float marker_scale_[MARKER_LIB_SIZE];
        tf2::Vector3 marker_map_[MARKER_LIB_SIZE];
        tf2::Vector3 obst_sizes_[MARKER_LIB_SIZE];

        // void poseCallback(const geometry_msgs::PoseStamped msg);
        void obstacleCallback(const aruco_msgs::MarkerArray msg);



};

#endif